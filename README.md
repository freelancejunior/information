# Freelance Junior
*We connect student freelancers with companies*

Because we got along so well, we got all kinds of jobs, until one day by chance we got a job that was better paid and even had something to do with our studies. "Why not immediately like that?", we asked ourselves to come to the conclusion a little later that on the one hand we didn't even know about the existence of these jobs and on the other hand we had just as little knowledge of taxes, bills and insurance - in short being independent in general. The same was true for Matze, an old sandbox friend who immediately joined our mission to change this.

Through meticulous research, which in terms of time spent eclipsed any housework done to date, we eventually had all the information together, only to find at the end that everything was structured and collected and no longer looked so scary. Free of the spirit of the Sharing Economy we decided to publish all the information collected, whereby only one of the two problems was solved. Where should all the new student freelancers find suitable assignments?

The idea of Freelance Junior was born. A platform that accompanies students on their first steps into self-employment and at the same time brings them together with clients for their first projects. While students benefit from their first practical experience, companies would also have the opportunity to discover talents at an early stage. If this isn't a win-win situation!


## Meta

Freelance Junior – [@FreelanceJunior](https://twitter.com/freelancejunior) – info@freelancejunior.de

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://github.com/freelancejunior/](https://github.com/freelancejunior/)


## Contributing

1. Fork it (https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request


## Sponsors


| Service | Description |
|---|---|
| [![](https://i.ibb.co/BZFP5Rg/atlassian.png)](https://atlassian.com) | Providing the Atlassian Jira Suite |
| [![](https://i.ibb.co/9W9g49h/browserstack.png)](https://browserstack.com) | Providing infrastructure for testing on multiple plattforms, devices and browsers |
| [![](https://i.ibb.co/xGnGL0h/bugsnag.png)](https://bugsnag.com) | Providing error monitoring |